#!/bin/bash

packer build wk-packer.json | tee out

ami="$(tail -n 1 out | cut -d ' ' -f 2)"

echo $ami
